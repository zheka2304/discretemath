from visualization import *
from task import *
from ui import *
from tkinter import simpledialog

import graph.saver as graph_saver
import json

from visualization.debug import *


if __name__ == "__main__":
    def _graph_saver(name, data):
        with open("extensions/saved_graphs.txt", "a+") as file:
            data = json.dumps(data)
            section = """\ngraph {\n    name = "%s"\n    data = '''%s'''\n}\n""" % (name, data)
            file.write(section)
    graph_saver.set_graph_data_saver(_graph_saver)

    __window = Window(resolution=(800, 500), surroundings=(250, 90), title="Тренажер на определения на графы (Альтернативный экзамен по ДМ 2019)")
    __window.environment.add_source("extensions/")

    __window.environment.visualizer.debug_window = GraphDebugWindow(__window.canvas,
                                                                    viewport=__window.environment.visualizer.screen)

    __window.mainloop()
