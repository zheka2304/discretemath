from .graph import *


# SAVER
def invalid_graph_data_saver(name, data):
    raise RuntimeError("no valid graph data saver registered")


graph_data_saver = invalid_graph_data_saver


def set_graph_data_saver(func):
    global graph_data_saver
    graph_data_saver = func


# REGISTRY
registered_graph_data = {}


def register_graph_data(name, data):
    registered_graph_data[name] = data


def load_graph(name):
    if name in registered_graph_data:
        graph = Graph()
        graph.deserialize(registered_graph_data[name])
        return graph
    else:
        raise NameError("cannot find graph to load: name = " + str(name))


def save_graph(name, graph):
    serialized = graph.serialize()
    register_graph_data(name, serialized)
    graph_data_saver(name, serialized)


def is_graph_registered(name):
    return name in registered_graph_data


__all__ = ["register_graph_data", "load_graph", "save_graph", "set_graph_data_saver", "is_graph_registered"]

