import tkinter as tk
from tkinter import *
from math import *
from time import sleep
from tkinter import messagebox
import json

import graph.saver as graph_saver


class CustomText(tk.Text):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # create a proxy for the underlying widget
        self._orig = self._w + "_orig"
        self.tk.call("rename", self._w, self._orig)
        self.tk.createcommand(self._w, self._proxy)

        self.tag_config('err',
                        foreground='#ff0000',
                        selectforeground="white"
                        )

    def get_text(self):
        return self.get("1.0", tk.END)

    def highlight(self, start, end, tag=None):
        self.tag_add(tag, start, end)

    def set_text(self, text):
        self.delete("1.0", tk.END)
        self.insert("1.0", text)

    def int_index(self, pos):
        result = self.tk.call(self, "count", "1.0", pos)
        if result == "":
            return 0
        return int(result)

    def _insert(self, position, chars, **kwargs):
        return False

    def _delete(self, start, end, **kwargs):
        return False

    def _replace(self, position):
        pass

    def _get_del_span(self, *args):
        if len(args) == 1:
            start = self.int_index(args[0])
            end = start + 1
        else:
            start = self.int_index(args[0])
            end = self.int_index(args[1])
        return start, end

    def _proxy(self, command, *args):
        prevent = False
        if command in ("insert", "delete"):
            pos = self.index(tk.INSERT)
            if command == "insert":
                prevent = self._insert(self.int_index(pos), *args[1:], str_pos=pos)
            if command == "delete":
                prevent = self._delete(*self._get_del_span(*args), str_span=args)
        result = ""
        if not prevent:
            try:
                result = self.tk.call((self._orig, command) + args)
            except tk.TclError:
                pass
        return result


class Console(CustomText):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.configure(
            bg="#ddd",
            fg="#444",
            insertbackground="#444",
            insertofftime=0,
            insertwidth=2,
            selectbackground="blue",
            selectforeground="white"
        )

        self._locked = 0

    def _write_and_lock(self, string, error=False, **flags):
        start = self.index("end-1c")
        self.tk.call(self._orig, "insert", tk.END, string)
        self._locked = self.int_index(tk.END) - 1
        if error:
            end = self.index("end-1c")
            self.highlight(start, end, tag="err")

    def to_end(self):
        self.see(tk.END)

    def write(self, string, newline=True, **flags):
        if len(string) == 0:
            return
        text = self.get_text()
        if newline and len(text) > 0 and text[-1] != "\n":
            string = "\n" + string
        self._write_and_lock(string + ("\n" if newline else ""), **flags)
        self.mark_set(tk.INSERT, tk.END)
        self.to_end()

    def error(self, string, **kwargs):
        self.write(string, error=True, **kwargs)

    def clear(self):
        self._locked = 0
        self.set_text("")

    def _delete(self, start, end, **kwargs):
        if start < self._locked:
            return True

    def _insert(self, position, chars, str_pos=None, **kwargs):
        return True


class LoadingSpinner(tk.Canvas):
    def __init__(self, *args, size=(50, 50), count=10, width=.075, shade=0xf0, **kwargs):
        self.size = size
        self.count = count
        self.shade = 0xf0
        self.width = width
        kwargs["width"], kwargs["height"] = size
        tk.Canvas.__init__(self, *args, **kwargs)

        w2 = size[0] / 2
        h2 = size[1] / 2
        start = .3
        end = .8
        width = max(1, width * (w2 * w2 + h2 * h2) ** .5)
        for i in range(count):
            ang = i * 2 * pi / count
            self.create_line((1 + sin(ang) * start) * w2, (1 + cos(ang) * start) * h2,
                             (1 + sin(ang) * end) * w2, (1 + cos(ang) * end) * h2,
                             tag=str(i), width=width, fill="white")
        self._frame = 0
        self._animating = False
        self.frame(0)

    def next_frame(self):
        self.after_idle(lambda: self.frame(self._frame + 1))

    def frame(self, frame):
        self._frame = frame
        for i in range(self.count + 1):
            b = int(((i + frame) % self.count) * self.shade / self.count)
            c = hex(b)[2:]
            while len(c) < 2:
                c = "0" + c
            self.itemconfig(str(i), fill="#" + c * 3)

    def start_animation(self, delay=50):
        def _animate():
            self.next_frame()
            if self._animating:
                self.after(delay, _animate)
        self._animating = True
        self.after_idle(_animate)

    def stop_animation(self):
        self._animating = False


class ExerciseDialog(Toplevel):
    def __init__(self):
        Toplevel.__init__(self)
        self.columnconfigure(0, weight=1)
        self.rowconfigure(5, weight=1)

        self.title("New Exercise")
        self.minsize(250, 300)
        self.maxsize(250, 300)
        self.geometry("250x300+350+150")
        self.attributes('-topmost', 'true')
        self.grab_set()
        self._definition_uuids = []

        self.bind("<Escape>", lambda event: self.cancel())
        self.bind("<Return>", lambda event: self.apply())

        description_message = Label(self, text="Setup your exercise:", font=("", 10, "bold"), anchor="w")
        description_message.grid(row=0, column=0, sticky="nsew")

        definition_frame = Frame(self)
        definition_frame.grid(row=5, column=0, sticky="nsew")
        definition_frame.rowconfigure(0, weight=1)
        definition_frame.columnconfigure(0, weight=1)

        self._definition_list = Listbox(definition_frame, selectmode='multiple')
        self._definition_list.grid(row=0, column=0, sticky="nsew")
        definition_scroll = Scrollbar(definition_frame, orient=VERTICAL)
        definition_scroll.grid(row=0, column=1, sticky="nsew")
        definition_scroll.config(command=self._definition_list.yview)
        self._definition_list.configure(yscrollcommand=definition_scroll.set)

        frame_count = Frame(self)
        frame_count.grid(row=1, column=0, sticky="nsew")
        frame_count.columnconfigure(1, weight=1)

        self._task_count_total = 10
        self._task_count_per_def = 3
        self._task_count = Scale(frame_count, from_=2, to=15, orient=HORIZONTAL, width=8)
        self._task_count.set(self._task_count_per_def)
        self._task_count.grid(row=0, column=1, sticky="nsew")
        self._task_count_label = Label(frame_count, text="\nTotal Tasks: ")
        self._task_count_label.grid(row=0, column=0, sticky="nsew")

        self._randomized = IntVar(value=1)
        self._randomize_checker = Checkbutton(self, text="Random definitions", variable=self._randomized,
                                              command=self._set_randomized)
        self._randomize_checker.grid(row=4, column=0, sticky="w")
        self._randomize_checker.select()

        self._set_randomized()

        buttons_frame = Frame(self)
        buttons_frame.grid(row=6, column=0, sticky="nsew")
        buttons_frame.columnconfigure(0, weight=1)
        buttons_frame.columnconfigure(1, weight=1)

        Button(buttons_frame, text="Cancel", command=self.cancel).grid(row=0, column=0, pady=4)
        btn_generate = Button(buttons_frame, text="Generate", command=self.apply)
        btn_generate.focus_set()
        btn_generate.grid(row=0, column=1, pady=4)

        self.complete = False
        self.result = None

        self.protocol("WM_DELETE_WINDOW", self.cancel)

    def _set_randomized(self):
        self.randomized = (self._randomized.get() == 1)
        if self.randomized:
            self._definition_list.configure(state=DISABLED)
            self._task_count_label.configure(text="\nTotal Tasks: ")
            self._task_count_per_def = self._task_count.get()
            self._task_count.configure(from_=2, to=15)
            self._task_count.set(self._task_count_total)
        else:
            self._definition_list.configure(state=NORMAL)
            self._task_count_label.configure(text="\nTasks Per Definition: ")
            self._task_count_total = self._task_count.get()
            self._task_count.configure(from_=1, to=5)
            self._task_count.set(self._task_count_per_def)

    def set_definitions(self, definitions):
        def do():
            self._definition_list.configure(state=NORMAL)
            self._definition_uuids = []
            self._definition_list.delete(0, "end")
            for uid, name in definitions.items():
                self._definition_uuids.append(uid)
                self._definition_list.insert("end", name)
            if self.randomized:
                self._definition_list.configure(state=DISABLED)
        self.after_idle(do)

    def cancel(self):
        self.complete = True
        self.result = None
        self.destroy()

    def apply(self):
        self.complete = True
        if self.randomized:
            task_count = self._task_count.get()
            if task_count < 6:
                definition_count = 2
            elif task_count < 10:
                definition_count = 3
            elif task_count < 12:
                definition_count = 4
            else:
                definition_count = 5
            self.result = (None, task_count, definition_count)
        else:
            definition_count = self._task_count.get()
            definitions = [self._definition_uuids[i] for i in self._definition_list.curselection()]
            self.result = (definitions, -1, definition_count)
        self.destroy()

    def get_result(self):
        while not self.complete:
            sleep(.1)
        return self.result


class SaveGraphDialog(Toplevel):
    def __init__(self, data=None):
        Toplevel.__init__(self)
        self.title("Save Graph")
        self.minsize(250, 300)
        self.maxsize(250, 300)
        self.geometry("250x300+350+150")
        self.attributes('-topmost', 'true')
        self.grab_set()

        self.columnconfigure(0, weight=1)
        self.rowconfigure(4, weight=1)

        Label(self, text="Save as (Name):").grid(row=0, column=0, sticky="nsw")

        def name_check(*args, **kwargs):
            name = self.name.get().strip()
            if len(name) == 0:
                self.invalid_name_label.configure(text="name is empty")
            elif len(name) == 0 or graph_saver.is_graph_registered(name):
                self.invalid_name_label.configure(text="name is occupied")
            else:
                self.invalid_name_label.configure(text="")
        self.name = StringVar()
        self.name.trace("w", name_check)

        self.data = data

        self.name_input = Entry(self, textvariable=self.name)
        self.name_input.grid(row=1, column=0, sticky="nsew")
        Label(self, text="Or copy data as text: ").grid(row=3, column=0, sticky="nsw")

        self.invalid_name_label = Label(self, text="", fg="red")
        self.invalid_name_label.grid(row=1, column=0, sticky="nse")

        text_frame = Frame(self)
        text_frame.grid(row=4, column=0, sticky="nsew")
        text_frame.columnconfigure(0, weight=1)
        text_frame.rowconfigure(0, weight=1)

        text = Text(text_frame)
        text.grid(row=0, column=0, sticky="nsew")
        text.insert("end", json.dumps(data))
        text.configure(state=DISABLED)

        bottom_frame = Frame(self)
        bottom_frame.grid(row=5, column=0, sticky="nsew")
        bottom_frame.columnconfigure(0, weight=1)
        bottom_frame.columnconfigure(1, weight=1)

        Button(bottom_frame, text="Cancel", command=self.cancel).grid(row=0, column=0, sticky="nsew")
        Button(bottom_frame, text="Save", command=self.apply).grid(row=0, column=1, sticky="nsew")

        self.complete = False
        self.result = None

        self.protocol("WM_DELETE_WINDOW", self.cancel)
        self.bind("<Escape>", lambda event: self.cancel())
        self.bind("<Return>", lambda event: self.apply())

    def is_name_valid(self):
        name = self.name.get().strip()
        return not(len(name) == 0 or graph_saver.is_graph_registered(name))

    def cancel(self):
        self.complete = True
        self.result = None
        self.destroy()

    def apply(self):
        if not self.is_name_valid():
            return
        self.result = self.name.get().strip()
        self.complete = True
        self.destroy()

    def get_result(self):
        while not self.complete:
            sleep(.1)
        return self.result


__all__ = ["Console", "LoadingSpinner", "ExerciseDialog", "SaveGraphDialog"]

if __name__ == '__main__':
    SaveGraphDialog().mainloop()
