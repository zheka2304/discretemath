import random


palettes = {
    "vertex_foreground": {
        "default": ["yellow", "red", "green"],
    },
    "vertex_highlight": {
        "default": ["blue", "#080", "red"],
    },
    "edge_foreground": {
        "default": ["red", "blue", "yellow"]
    },
    "edge_highlight": {
        "default": ["#0f0", "#0f0", "#0f0"]
    }
}


def get_color_from_palette(family, palette, index):
    family = palettes[family]
    if palette in family:
        palette = family[palette]
    else:
        palette = family["default"]
    while index >= len(palette):
        color = [random.randint(0, 255), random.randint(0, 255)]
        base = max(*color)
        color.append(255 - random.randint(max(0, base - 50), min(255, base + 50)))
        random.shuffle(color)
        palette.append("#%0.2X%0.2X%0.2X" % tuple(color))
    return palette[index]

