from tkinter import *
import uuid
from time import sleep, perf_counter
from threading import Thread


class GraphDebugWindow:
    def __init__(self, canvas, viewport=(100, 100, 500, 500), threaded=False):
        self.viewport = viewport
        self.canvas = canvas
        self.threaded = threaded

        self._last_edges = None
        self._redraw_thread = None
        self._redraw_queue = []
        self._drawing = False

    def _queue(self, func):
        if not self.threaded:
            func()
            return
        if self._redraw_thread is None:
            def loop():
                idle_frames = 0
                while idle_frames < 5:
                    # if self._drawing:
                    #     while self._drawing:
                    #         sleep(.05)
                    #     sleep(.0)  # frame skip
                    if len(self._redraw_queue) > 0:
                        def make_drawer(f):
                            def drawer():
                                self._drawing = True
                                f()
                                self._drawing = False
                            return drawer
                        self._redraw_queue = [self._redraw_queue[-1]]
                        self.canvas.after_idle(make_drawer(self._redraw_queue.pop(0)))
                        idle_frames = 0
                    else:
                        idle_frames += 1
                    sleep(.01)  # frame skip
                self._redraw_thread = None
            self._redraw_thread = Thread(target=loop)
            self._redraw_thread.start()
        self._redraw_queue.append(func)

    def draw_physics(self, edges, state):
        start = perf_counter()
        vertices = []
        for vertex in state:
            vertices.append(vertex[0])
            vertices.append(vertex[1])
        self.draw(edges, vertices)
        return perf_counter() - start

    def draw(self, edges, vertices):
        start = perf_counter()
        borders = [vertices[0], vertices[1], vertices[0], vertices[1]]
        for i in range(len(vertices) // 2):
            x, y = vertices[i * 2], vertices[i * 2 + 1]
            if borders[0] > x:
                borders[0] = x
            if borders[1] > y:
                borders[1] = y
            if borders[2] < x:
                borders[2] = x
            if borders[3] < y:
                borders[3] = y

        lines = []
        for edge in edges:
            i, j = edge.start.index, edge.end.index
            x1 = (vertices[i * 2] - borders[0]) / (borders[2] - borders[0]) * (self.viewport[2] - self.viewport[0]) + self.viewport[0]
            y1 = (vertices[i * 2 + 1] - borders[1]) / (borders[3] - borders[1]) * (self.viewport[3] - self.viewport[1]) + self.viewport[1]
            x2 = (vertices[j * 2] - borders[0]) / (borders[2] - borders[0]) * (self.viewport[2] - self.viewport[0]) + self.viewport[0]
            y2 = (vertices[j * 2 + 1] - borders[1]) / (borders[3] - borders[1]) * (self.viewport[3] - self.viewport[1]) + self.viewport[1]
            lines.append((x1, y1, x2, y2))

        def redraw():
            color = "#999"
            tag = "debug"
            self.canvas.delete(tag)
            for line in lines:
                self.canvas.create_line(*line, fill=color, tag=tag)
        self._queue(redraw)
        return perf_counter() - start

    def clear(self):
        tag = "debug"
        self._redraw_queue = []
        self._queue(lambda: self.canvas.delete(tag))



