

def segment_intersect(x1, y1, x2, y2, x3, y3, x4, y4):
    if x1 == x2 or x3 == x4:
        return None
    a1 = (y1 - y2) / (x1 - x2)
    a2 = (y3 - y4) / (x3 - x4)
    b1 = y1 - a1 * x1
    b2 = y3 - a2 * x3
    if a1 == a2:
        return None
    xa = (b2 - b1) / (a1 - a2)
    ya = a1 * xa + b1
    if (xa < max(min(x1, x2), min(x3, x4))) or (xa > min(max(x1, x2), max(x3, x4))):
        return None
    else:
        return xa, ya


def segment_length_sqr(x1, y1, x2, y2):
    return (x1 - x2) ** 2 + (y1 - y2) ** 2


def segment_distance_sqr(x1, y1, x2, y2, x3, y3, return_intersect=False):
    l2 = segment_length_sqr(x1, y1, x2, y2)
    if abs(l2) < 1e-10:
        return segment_length_sqr(x1, y1, x3, y3)
    t = max(0, min(1, ((x3 - x1) * (x2 - x1) + (y3 - y1) * (y2 - y1)) / l2))
    x = x1 + t * (x2 - x1)
    y = y1 + t * (y2 - y1)
    if return_intersect:
        return x, y, t
    else:
        return segment_length_sqr(x, y, x3, y3)


def pack_kwargs(**kwargs):
    return kwargs
