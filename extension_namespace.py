from graph import *

import random
import json
import task.definition as _definitions
from task.action import *
import visualization.unit as graphics

from graph.saver import load_graph as load_graph_internal


def load_graph(name):
    graph = load_graph_internal(name)
    graph.set_all_fixed(True)
    return graph


def unwrap_graph(string):
    graph = Graph()
    graph.deserialize(json.loads(string))
    graph.set_all_fixed(True)
    return graph


#
def _action_set_description(*flags, **kwargs):
    docs = {}
    build_action_set(*flags, docs=docs, **kwargs)
    return build_action_set_description(docs)


ACTION = build_action
ACTION_SET = build_action_set
ACTION_SET_DESCRIPTION = _action_set_description


#
def definition(*args, **kwargs):
    return _definitions.make_definition(*args, **kwargs)
