import uuid

from .compiler import *
from .definition import *
from .action import *


class Extension:
    def __init__(self):
        self.root = ParametrizedSection()
        self.categories = {}

        self._source = None
        self._file = None
        self._compiled = False
        self._initialized = False
        self._error = None
        self._namespace = None

    def _initialize_category(self, name, clazz):
        self.categories[name] = list(self.search_all(lambda section: isinstance(section, clazz)))

    def get_category(self, name):
        if name in self.categories:
            return self.categories[name]
        return []

    def initialize(self, namespace=None, **kwargs):
        if not self._compiled:
            raise RuntimeError("extension is not compiled")
        if self._initialized:
            raise RuntimeError("extension already initialized")

        if namespace is None:
            import extension_namespace
            namespace = vars(extension_namespace)
        try:
            for subsection in self.root.subsections:
                subsection.initialize(dict(namespace), extension=self, **kwargs)
            self._namespace = namespace
            self._initialize_category("tasks", SectionTask)
            self._initialize_category("modules", SectionModule)
            self._initialize_category("definitions", SectionDefinition)
            self._initialize_category("graphs", SectionGraph)
            self._initialized = True
        except RuntimeError as err:
            self._error = err

    def compile(self, source=None, file=None):
        if source is None:
            if file is None:
                raise RuntimeError("nor source or file are given")
            if isinstance(file, str):
                self._file = file
                with open(file, "r", encoding="utf-8") as f:
                    source = f.read()
                    f.close()
            else:
                source = file.read()

        if self._compiled:
            raise RuntimeError("extension is already compiled")
        try:
            self._source = source
            self.root.parse(source)
            self._compiled = True
        except Exception as err:
            self._error = err

    def action(self, name, **kwargs):
        return self.root.action(name, **kwargs)

    def search(self, checker):
        return self.root.search(checker)

    def search_all(self, checker):
        return self.root.search_all(checker)

    def file(self):
        return self._file

    def filename(self):
        return re.split("[/\\\\]", str(self._file))[-1]

    def compiled(self):
        return self._compiled

    def initialized(self):
        return self._initialized

    def valid(self):
        return self._error is None

    def throw(self):
        if self._error is not None:
            raise self._error

    def error(self):
        return self._error

    def namespace(self):
        return self._namespace


class Task:
    def __init__(self):
        self.uuid = str(uuid.uuid4())
        self.procedural = False        # mark for procedural-generated task, such task would not be cleared on reload
        self.combined = False          # mark for tasks, generated from combined definitions
        self.anonymous = False         # mark for tasks, that will not appear in task list
        self._user_solutions = []

    def name(self):
        raise NotImplementedError()

    def description(self):
        raise NotImplementedError()

    def action(self, name, *args, wrap=lambda x: x, **kwargs):
        raise NotImplementedError()

    def generate_condition(self):
        raise NotImplementedError()

    def update(self, condition):
        raise NotImplementedError()

    def convert(self, condition):
        raise NotImplementedError()

    def validate(self, solution, **kwargs):
        raise NotImplementedError()

    def randomize_parameters(self):
        pass

    def forget_solutions(self):
        self._user_solutions = []

    def add_user_solution(self, solution):
        self._user_solutions.append(solution)

    def is_solved(self):
        return len(self._user_solutions) > 0

    def get_solutions(self):
        return self._user_solutions


# Wrapper of initialized task section
class TaskWrap(Task):
    def __init__(self, section, manager):
        Task.__init__(self)

        self.section = section
        self.manager = manager

        self._action = section.search(lambda x: isinstance(x, SectionAction))
        self._solution = section.search(lambda x: isinstance(x, SectionSolution))

    def name(self):
        name = self.section.variable("name")
        return self.uuid if name is None else name

    def description(self):
        description = self.section.variable("description")
        return "No Description Provided" if description is None else description

    def action(self, name, *args, wrap=lambda x: x, **kwargs):
        if self._action is not None:
            action = self._action.variable(name)
            if action:
                return wrap(action(*args, **kwargs))
        return wrap(None)

    def generate_condition(self):
        func = self._solution.variable("generate")
        if func is None:
            raise RuntimeError("task " + self.name() + " has no generate() method")
        condition = func()
        if not isinstance(condition, Graph):
            raise RuntimeError("task generate() method must return Graph, not " + str(condition))
        return condition

    def update(self, condition):
        func = self._solution.variable("update")
        if func is not None:
            func(condition)

    def convert(self, condition):
        func = self._solution.variable("convert")
        if func is not None:
            condition = func(condition)
        if not isinstance(condition, Graph):
            raise RuntimeError("task convert() method must return Graph, not " + str(condition))
        return condition

    def validate(self, condition, **kwargs):
        func = self._solution.variable("validate")
        if func is None:
            raise RuntimeError("task " + self.name() + " has no validate(condition) method")
        return bool(func(condition))


# Wraps definition into task
class DefinitionTask(Task):
    def __init__(self, definition):
        Task.__init__(self)

        self.definition = definition
        self.params = self.definition.generate_parameters()

        self._flags = list(self.definition.get_flags())

        self._action_description = {}
        self._update_action_set()

        if FLAG_NO_TASK in self._flags:
            self.anonymous = True

        self._checked = None
        self._name = None

    def set_name(self, name):
        self._name = name

    def randomize_parameters(self):
        self.params = self.definition.generate_parameters()

    def check(self):
        if self._checked is None:
            self._checked = self.generate_condition() is not None
        return self._checked

    def trusted(self):
        return FLAG_TRUSTED in self._flags

    def name(self):
        if self._name is not None:
            return self._name
        return self.definition.title()

    def description(self):
        intro = "Make a graph"
        if ACTION_EDIT in self._flags and ACTION_PAINT in self._flags:
            intro = "Build and paint a graph"
        else:
            if ACTION_EDIT in self._flags:
                intro = "Build a graph"
            if ACTION_PAINT in self._flags:
                intro = "Paint given graph"
        intro += ", to satisfy given definition(s):\n\n"

        return intro + self.definition.description(**self.params) + "\n\nControls:\n" + \
               build_action_set_description(self._action_description)

    def action(self, name, *args, wrap=lambda x: x, **kwargs):
        if name in self._actions and self._actions[name] is not None:
            self._actions[name](*args, **kwargs)

    def generate_condition(self):
        def convert_count(x, factor=1., point=1):
            return int(((x / point) ** factor) * point)
        condition = self.definition.generate_example(**self.params)
        if condition is not None:
            if not self.definition.apply(condition, original=condition, **self.params):
                print("internal error: task might not be solvable: " + self.name())
            condition = condition.copy()
            if ACTION_EDIT in self._flags:
                if FLAG_NO_EDGES not in self._flags:
                    edges = convert_count(len(condition.get_edges()), factor=.5, point=7)
                    edges = random.randint(edges // 2, edges // 1)
                    vertices = convert_count(len(condition) / 3, factor=.4, point=3)
                    vertices = random.randint(0, vertices)
                    if FLAG_NO_VERTICES not in self._flags:
                        condition.add_random_vertices(vertices)
                        condition.add_random_edges(edges)
                        if FLAG_ADD_COMPONENTS in self._flags and FLAG_NO_COMPONENTS not in self._flags:
                            while random.random() < .5:
                                condition.add_random_components(1)
                    else:
                        condition.add_random_edges(edges)
            if ACTION_PAINT in self._flags:
                def reset_color(x):
                    if not x.locked:
                        x.color = 0
                # calculate max colors
                max_vertex_colors = max(condition.get_vertex_colors()) + 1
                cur_vertex_colors = get_flag_value(self._flags, SET_VERTEX_COLORS)
                if (cur_vertex_colors is None or cur_vertex_colors < max_vertex_colors) and max_vertex_colors > 1:
                    self._flags.append(SET_VERTEX_COLORS(max_vertex_colors))
                    self._update_action_set()
                max_edge_colors = max(condition.get_edge_colors()) + 1
                cur_edge_colors = get_flag_value(self._flags, SET_EDGE_COLORS)
                if (cur_edge_colors is None or cur_edge_colors < max_edge_colors) and max_edge_colors > 1:
                    self._flags.append(SET_EDGE_COLORS(max_edge_colors))
                    self._update_action_set()
                # reset colors
                condition.for_vertices(reset_color)
                condition.for_edges(reset_color)
            if ACTION_DIRECT in self._flags:
                def reset_directed(x):
                    if not x.locked:
                        x.directed = False
                        if random.random() < .5:
                            x.redirect()
                condition.for_edges(reset_directed)

            if ACTION_EDIT in self._flags:
                condition.set_active(FLAG_ALL_ACTIVE in self._flags)
            condition = condition.copy()
        return condition

    def _update_action_set(self):
        self._action_description = {}
        self._actions = build_action_set(*self._flags, priorities=DEFAULT_ACTION_PRIORITY_TABLE,
                                         docs=self._action_description)

    def update(self, condition):
        pass

    def convert(self, condition):
        return condition.from_active()

    def validate(self, solution, **kwargs):
        return self.definition.apply(solution, **self.params, **kwargs)


class Exercise:
    def __init__(self, definitions):
        self.definitions = definitions
        self.tasks = []

    def build(self, tasks_per_definition=None, total_tasks=10, max_tries=10):
        self.tasks = []
        remainder = 0
        if tasks_per_definition is None:
            tasks_per_definition = total_tasks // len(self.definitions)
            remainder = total_tasks % len(self.definitions)
        for i in range(len(self.definitions)):
            definition = self.definitions[i]
            used_params_sets = []
            index = 1
            for j in range(tasks_per_definition + (1 if i < remainder else 0)):
                task = DefinitionTask(definition)
                task.anonymous = True
                task.procedural = True
                tries = 0
                while task.params in used_params_sets or not (task.trusted() or task.check()):
                    task.randomize_parameters()
                    tries += 1
                    if tries >= max_tries:
                        break
                else:
                    task.set_name(definition.title() + " #" + str(index))
                    if len(task.params) > 0:
                        used_params_sets.append(task.params)
                    self.tasks.append(task)
                    index += 1

    def is_solved(self):
        for task in self.tasks:
            if not task.is_solved():
                return False
        return True


class ExtensionManager:
    def __init__(self):
        self.extensions = []
        self.tasks = {}
        self.definitions = {}

        self._available_cross_definitions = None

    def clear(self, procedural=False):
        self.extensions = []
        self.tasks = {key: val for key, val in self.tasks.items() if not procedural and val.procedural}
        self.definitions = {key: val for key, val in self.definitions.items() if not procedural and val.procedural}

    # clears only procedural tasks
    def clear_procedural_tasks(self):
        self.tasks = {key: val for key, val in self.tasks.items() if not val.procedural}

    # clears only procedural definitions
    def clear_procedural_definitions(self):
        self._available_cross_definitions = None
        self.definitions = {key: val for key, val in self.definitions.items() if not val.procedural}

    def load(self, silent=False, **kwargs):
        extension = Extension()
        extension.compile(**kwargs)
        if not silent:
            extension.throw()
        self.extensions.append(extension)

    def get_task_uuids(self):
        def key_prefix(task):
            if task.procedural:
                if task.combined:
                    return "\uffff"
                return "\ufffe"
            return ""
        uuids = list(self.tasks.items())
        uuids.sort(key=lambda x: key_prefix(x[1]) + x[1].name())
        return list(map(lambda x: x[0], uuids))

    def get_definition_uuids(self):
        def key_prefix(definition):
            if not definition.validated:
                return "\xff"
            if definition.procedural:
                return "\xfe"
            return ""
        uuids = list(self.definitions.items())
        uuids.sort(key=lambda x: key_prefix(x[1]) + x[1].name())
        return list(map(lambda x: x[0], uuids))

    def _load_tasks_from_definitions(self, extension):
        pass

    def initialize_extension(self, extension, silent=False, **kwargs):
        if extension.compiled():
            extension.initialize(manager=self, **kwargs)
        if not silent:
            extension.throw()
        if extension.initialized():
            for section in extension.get_category("tasks"):
                task = TaskWrap(section, self)
                self.tasks[task.uuid] = task
            for section in extension.get_category("definitions"):
                definition = ExtensionDefinition(section, self)
                self.definitions[definition.uuid] = definition

    def initialize(self, **kwargs):
        for extension in self.extensions:
            if not extension.initialized():
                self.initialize_extension(extension, **kwargs)

    def get_all_errors(self):
        return [extension.error() for extension in self.extensions if not extension.valid()]

    def import_module(self, name, namespace, caller=None, **kwargs):
        found = False
        for extension in self.extensions:
            module = extension.search(lambda section:
                                       isinstance(section, SectionModule) and
                                       section.name == name)
            if module is not None:
                module.initialize(namespace, manager=self, extension=extension, **kwargs)
                found = True
        return found

    # creates, registers and returns task from given definition
    def task_from_definition(self, definition):
        task = DefinitionTask(definition)
        task.procedural = True
        print("generating procedural task: " + task.name())
        if task.trusted() or task.check():
            definition.validated = True
            self.tasks[task.uuid] = task
            return task
        else:
            definition.validated = False

    # re-wraps all definition into tasks
    def generate_procedural_tasks(self):
        self.clear_procedural_tasks()
        for definition in self.definitions.values():
            self.task_from_definition(definition)

    # generates single definition based on given ones and save it on success
    def generate_cross_definition(self, *definitions, scheduler=None, tries=128):
        for definition in definitions:
            if FLAG_NO_CROSS in definition.get_flags():
                return
        definition = DefinitionCombination(*definitions, cross_solution_iterations=16, name_separator=" & ")
        definition.procedural = True
        if definition.solve(tries=tries, scheduler=scheduler):
            self.definitions[definition.uuid] = definition
            return definition

    # tries to generate next unique random cross-definition
    def generate_random_cross_definition(self, **kwargs):
        if self._available_cross_definitions is None:
            def def_combination_list(defs, combination_size, prefix=(), start=0):
                if combination_size < 1:
                    return [prefix]
                combinations = []
                for i in range(start, len(defs)):
                    definition = defs[i]
                    if not definition.procedural and definition.validated:
                        combinations += def_combination_list(defs, combination_size - 1, (*prefix, definition),
                                                             start=i + 1)
                return combinations
            self._available_cross_definitions = def_combination_list(list(self.definitions.values()), 2)

        while len(self._available_cross_definitions) > 0:
            definitions = self._available_cross_definitions.pop(0)
            print("generating cross definition:", *(definition.name() for definition in definitions))
            definition = self.generate_cross_definition(*definitions, **kwargs)
            if definition is not None:
                return definition


