# action flags
TARGET_VERTEX = "TARGET_VERTEX"              # Targets vertices
TARGET_EDGE = "TARGET_EDGE"                  # Targets edge

ACTION_EDIT = "ACTION_EDIT"                  # Allow to activate/deactivate
ACTION_PAINT = "ACTION_PAINT"                # Allow to change color
ACTION_DIRECT = "ACTION_DIRECT"              # Allow to change edge directions
ACTION_DRAG = "ACTION_DRAG"                  # Allow to drag edges

# flags
FLAG_NO_TASK = "FLAG_NO_TASK"                # do not generate task from definition
FLAG_NO_CROSS = "FLAG_NO_CROSS"              # do not generate cross definitions
FLAG_TRUSTED = "FLAG_TRUSTED"                # do not check this task
FLAG_ADD_COMPONENTS = "FLAG_ADD_COMPONENTS"  # condition generator may add new components to graph
FLAG_NO_COMPONENTS = "FLAG_NO_COMPONENTS"    # condition generator will not add new components, oppose ADD_COMPONENTS
FLAG_NO_VERTICES = "FLAG_NO_VERTICES"        # condition generator wont add new vertices
FLAG_NO_EDGES = "FLAG_NO_EDGES"              # condition generator wont add new vertices and edges
FLAG_ALL_ACTIVE = "FLAG_ALL_ACTIVE"          # activate all possible elements instead of deactivation

# bindings
BINDING_VERTEX_SELECT = "vertex_select"
BINDING_VERTEX_SWITCH = "vertex_switch"
BINDING_VERTEX_DOUBLE = "vertex_double"
BINDING_VERTEX_DRAG = "vertex_drag"
BINDING_EDGE_SELECT = "edge_select"
BINDING_EDGE_SWITCH = "edge_switch"
BINDING_EDGE_DOUBLE = "edge_double"
BINDING_EDGE_DRAG = "edge_drag"


# other constants TODO: update if required
MAX_VERTEX_COLORS = 3
MAX_EDGE_COLORS = 3


# built-in data tables
DEFAULT_ACTION_PRIORITY_TABLE = {
    ACTION_EDIT: {
        TARGET_VERTEX: {
            "priority": 0,
            "bindings": [BINDING_VERTEX_SELECT],
        },
        TARGET_EDGE: {
            "priority": 0,
            "bindings": [BINDING_EDGE_SELECT, BINDING_EDGE_DOUBLE]
        }
    },
    ACTION_PAINT: {
        TARGET_VERTEX: {
            "condition": lambda flags: get_flag_value(flags, SET_VERTEX_COLORS) is not None,
            "priority": 3,
            "bindings": [BINDING_VERTEX_SWITCH]
        },
        TARGET_EDGE: {
            "condition": lambda flags: get_flag_value(flags, SET_EDGE_COLORS) is not None,
            "priority": 3,
            "bindings": [BINDING_EDGE_SWITCH]
        },
    },
    ACTION_DIRECT: {
        TARGET_EDGE: {
            "priority": 1,
            "bindings": [BINDING_EDGE_SWITCH, BINDING_EDGE_SELECT]
        },
    },
    ACTION_DRAG: {
        TARGET_VERTEX: {
            "priority": -1,
            "bindings": [BINDING_VERTEX_DRAG]
        },
    }
}

BINDING_DESCRIPTION = {
    BINDING_VERTEX_SELECT: "Left-click vertex",
    BINDING_VERTEX_SWITCH: "Right-click vertex",
    BINDING_VERTEX_DOUBLE: "Double-click vertex",
    BINDING_VERTEX_DRAG: "Drag vertex",
    BINDING_EDGE_SELECT: "Left-click edge",
    BINDING_EDGE_SWITCH: "Right-click edge",
    BINDING_EDGE_DOUBLE: "Double-click edge",
    BINDING_EDGE_DRAG: "Drag edge",
}

ACTION_DESCRIPTION = {
    ACTION_EDIT: "activate/deactivate",
    ACTION_PAINT: "change color",
    ACTION_DIRECT: "add/change direction",
    ACTION_DRAG: "drag allowed"
}


# kwargs flags: [name, value, conflict resolve scheme]
SET_VERTEX_COLORS = lambda x: ["vertex_colors", x, max]   # Sets count of vertex colors
SET_EDGE_COLORS = lambda x: ["edge_colors", x, max]       # Sets count of edge colors

# global variables in one dict
__action_globals = {
    "direct_edges_uuids": []
}


def _parse_action_tuple(*actions):
    flags = []
    params = {}
    for action in actions:
        if isinstance(action, list):
            if action[0] in params:
                params[action[0]] = action[2](params[action[0]], action[1])
            else:
                params[action[0]] = action[1]
        else:
            flags.append(action)
    params["flags"] = tuple(flags)
    return params


def get_flag_value(flags, flag):
    kwargs = _parse_action_tuple(*flags)
    flag = flag(0)[0]
    if flag in kwargs:
        return kwargs[flag]


def build_action(*args, filter=None):
    kwargs = _parse_action_tuple(*args)
    return _build_action(filter=filter, **kwargs)


def build_action_set(*flags, priorities=None, docs=None):
    if docs is None:
        docs = {}
    table = {}
    for flag in flags:
        try:
            if flag in priorities:
                targets = priorities[flag]
                for target in targets:
                    if "condition" in targets[target]:
                        if not targets[target]["condition"](flags):
                            continue
                    bindings = targets[target]["bindings"]
                    priority = targets[target]["priority"]
                    table[(flag, target)] = priority, bindings[0], bindings[1:]
        except TypeError:  # unhashable type
            pass
    order = list(table.keys())
    order.sort(key=lambda k: -table[k][0])  # sort keys by priority

    for key in order:
        priority, binding, bindings = table[key]
        while len(bindings) > 0:
            for key_ in order:
                priority_, binding_, bindings_ = table[key_]
                if (len(bindings_) == 0 or priority_ > priority) and binding_ == binding:
                    binding = bindings.pop(0)
                    # print(key_, "overflowed", key, "for", binding_, "changed to", binding)
                    break
            else:
                break
        table[key] = priority, binding, bindings

    actions = {}
    for key in table:
        priority, binding, bindings = table[key]
        if binding not in actions:
            actions[binding] = build_action(*key, *flags, filter=key)
            docs[binding] = key
        else:
            print("failed to create unique binding for", key)
    return actions


def build_action_set_description(description, action_table=None, binding_table=None):
    if binding_table is None:
        binding_table = BINDING_DESCRIPTION
    if action_table is None:
        action_table = ACTION_DESCRIPTION
    string = ""
    for binding, key in description.items():
        action = key[0]
        if binding in binding_table:
            binding = binding_table[binding]
        if action in action_table:
            action = action_table[action]
        string += "{} - {}\n".format(binding, action)
    return string


def _build_action(flags=(), filter=None, **kwargs):
    flags = [flag for flag in flags if filter is None or flag in filter]
    if TARGET_VERTEX in flags:
        if ACTION_EDIT in flags:
            return _build_vertex_edit_action(flags=flags, **kwargs)
        if ACTION_PAINT in flags:
            return _build_vertex_paint_action(flags=flags, **kwargs)
        if ACTION_DRAG in flags:
            return _build_vertex_drag_action(flags=flags, **kwargs)
    if TARGET_EDGE in flags:
        if ACTION_EDIT in flags:
            return _build_edge_edit_action(flags=flags, **kwargs)
        if ACTION_PAINT in flags:
            return _build_edge_paint_action(flags=flags, **kwargs)
        if ACTION_DIRECT in flags:
            return _build_edge_direct_action(flags=flags, **kwargs)


def _build_vertex_edit_action(flags=(), **kwargs):
    def action_vertex_edit(vertex, unit, **kw):
        if vertex.locked:
            return
        if vertex.active:
            for edge in vertex.edges:
                if edge.locked:
                    return

        vertex.active = not vertex.active
        if not vertex.active:
            for edge in vertex.edges:
                edge.active = False
    return action_vertex_edit


def _build_vertex_paint_action(flags=(), vertex_colors=1, **kwargs):
    def vertex_paint_action(vertex, unit, **kw):
        if vertex.locked:
            return
        if vertex.active:
            vertex.color = (vertex.color + 1) % vertex_colors
    return vertex_paint_action


def _build_vertex_drag_action(flags=(), **kwargs):
    def vertex_drag_action(vertex, unit, position=None, **kw):
        vertex.position = unit.position = position
        unit.invalidate()
    return vertex_drag_action


def _build_edge_edit_action(flags=(), **kwargs):
    def edge_edit_action(edge, unit, **kw):
        if edge.locked:
            return
        edge.active = not edge.active
        start, end = edge.start, edge.end
        if edge.active:
            if not start.locked:
                start.active = True
            if not end.locked:
                end.active = True
        else:
            def _check_vertex(v):
                edges = 0
                for e in v.edges:
                    if e.active:
                        edges += 1
                if edges == 0:
                    if not v.locked:
                        v.active = False

            _check_vertex(start)
            _check_vertex(end)
    return edge_edit_action


def _build_edge_paint_action(flags=(), edge_colors=1, **kwargs):
    def edge_paint_action(edge, unit, **kw):
        if edge.locked:
            return
        if edge.active:
            edge.color = (edge.color + 1) % edge_colors
    return edge_paint_action


def _build_edge_direct_action(flags=(), **kwargs):
    def edge_direct_action(edge, unit, **kw):
        if edge.locked:
            return
        if edge.active:
            uuids = __action_globals["direct_edges_uuids"]
            if edge.directed:
                edge.redirect()
                if edge.uuid in uuids:
                    uuids.remove(edge.uuid)
                    edge.directed = False
                else:
                    uuids.append(edge.uuid)
            else:
                edge.directed = True
    return edge_direct_action

